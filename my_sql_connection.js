const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');

const app = express();  

app.use(bodyParser.json()); 

//db connection
const conn = mysql.createConnection({
    host: 'localhost', user: 'root', password: 'root', database: 'restful_db'
});

//connecting to database
conn.connect((err) => {
    if(err) throw err;
    console.log('MYSQL CONNECTED SUCCEFULLY');
})