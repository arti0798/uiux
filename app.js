var express = require('express');
var app = express();

app.get('/', function(req, res){
    res.send('HELLO WORLD');
})

app.get('/msg', function(req, res) {
    res.send('GOOD MORNING')
});

app.listen(3000, function() {
    console.log('APP LISTING 3000')
});